/*
  Przedmiot: ARKO.
  Symulacja toru lotu szybowca
  Patryk Szypulski
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include <cstring>
#include <iostream>
		
struct parametry {	
	int zarezerwowane;
	int szer;				
	int wys;						// szerokość i wysokość bitmapy w pikselach
	int czestotliwosc;		
	double czas_probk;				// czas próbkowania i czestotliwosc wyświetlania
	double vx_pocz;			
	double vy_pocz;					// początkowe prędkości pionowa i pozioma w px/s [skala 1m/px]    
	int x_pocz;				
	int y_pocz;						// początkowe wartości położenia względem dolnego lewego rogu w px
	double stala_oporu;		
	double stala_nosna;				// stałe skalujące siły działające na szybowiec
};
typedef struct parametry parametry;

static parametry params {/*zarezerwowane 4 bajty */ 1, /*szer*/ 1300, /*wys*/ 1000, /*wyswiet*/ 0, /*probk*/ 0.001,
                         /*vx_pocz*/ 50.0, /*vy_pocz*/ 120.0, /*x_pocz*/ 240, /*y_pocz*/ 520,
                         /*oporu*/ 0.001, /*nosna*/ 10.5 };
extern "C" double tor(char *tab, parametry *p);

void wyswietl_param() {
	std::cout << "Vx_pocz=" << params.vx_pocz <<", Vy_pocz="<< params.vy_pocz
	<<", x_pocz="<<params.x_pocz<<", y_pocz="<<params.y_pocz<<"\nprobkowanie="<<params.czas_probk<<", wyswietlanie="<<params.czestotliwosc
	<<"\nstala_oporu="<<params.stala_oporu<<", stala_nosna="<<params.stala_nosna<<std::endl << std::endl;
}
void Display( void )				
//Funkcji odpowiedzialna za wyswietlanie zawartosci okna
{
    static bool bBusy=false;
				
    if (bBusy) return;
    bBusy=true;
	
	char *pixels = new char[3*params.szer*params.wys];			
		 
	//The glClear function clears buffers to preset values.
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );						    
    
    
    int size = 3*params.wys*params.szer;
	for (int i=0; i < size; ++i) {
		
		pixels[i] = 255;
	}
	tor (pixels, &params);
	glDrawPixels(params.szer,params.wys,GL_RGB,GL_UNSIGNED_BYTE,pixels);
	wyswietl_param();
	glutSwapBuffers();
	delete pixels;
	bBusy = false;
}
							 
								 
static void Reshape( int width, int height )
{
    glViewport( 0, 0, width, height); //wsp. rzutni w oknie
}

static void Key( unsigned char key, int x, int y )
 //Obsluga klawiatury (znaki ASCII)
 //x,y - wspolrzedne polozenia myszy
{
	const double krok_predkosci = 0.5;
	const double krok_stalej_nosnej = 0.01;
	const double krok_stalej_oporu = 0.001;
	switch (key) {
		case '+':
			++params.czestotliwosc;
			break;
		case '-':
			if ( params.czestotliwosc > 0)
				--params.czestotliwosc;	
			break;	
		case 'q':
			params.vx_pocz += krok_predkosci;
			break;
		case 'a':
			params.vx_pocz -= krok_predkosci;
			break;
		case 'w':
			params.vy_pocz += krok_predkosci;
			break;
		case 's':
			params.vy_pocz -= krok_predkosci;
			break;
		case 'e':
			params.stala_nosna += krok_stalej_nosnej;
			break;
		case 'd':
			params.stala_nosna -= krok_stalej_nosnej;
			break;		
		case 'r':
			params.stala_oporu += krok_stalej_oporu;
			break;
		case 'f':
			params.stala_oporu -= krok_stalej_oporu;
			if (params.stala_oporu < 0)
				params.stala_oporu = 0;
			break;
	}
	glutPostRedisplay(); //Zawiadomienie, ze nalezy odswiezyc zawartosc okna
}

static void SpecialKey( int key, int x, int y )
//Obsluga klawiatury (klawisze funkcyjne + strzalki + kl. specjalne)
//x,y - wspolrzedne polozenia myszy
{
	switch (key) {
		case GLUT_KEY_UP:
			++params.y_pocz;
			break;
		case GLUT_KEY_DOWN:
			--params.y_pocz;
			break;
		case GLUT_KEY_LEFT:
			--params.x_pocz;
			break;
		case GLUT_KEY_RIGHT:
			++params.x_pocz;
			break;
	}
	glutPostRedisplay(); //Zawiadomienie, ze nalezy odswiezyc zawartosc okna
}

static void Init( void )
{
glEnable(GL_DEPTH_TEST);    
}

int main( int argc, char *argv[] )
{
//Inicjalizacja biblioteki GLUT
glutInit( &argc, argv );
//Poczatkowa pozycja okna wyrazona w pikselach
glutInitWindowPosition( 0, 0 );
//Poczatkowy rozmiar okna wyrazony w pikselach
glutInitWindowSize( params.szer, params.wys );
//Tryb wyswietlania:
//kolory: GLUT_RGB - tryb RGB, GLUT_INDEX - kolory indeksowane
//buforowanie: GLUT_DOUBLE - podwojne buforowanie, GLUT_SINGLE - pojedyncze buforowanie
//GLUT_DEPTH - okno z buforem glebokosci
glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
//Utworzenie okna
glutCreateWindow("Symulacja toru lotu szybowca");
//Rejestracja funkcji odpowiedzialnej za zmiane parametrow
//po zmianie rozmiaru okna
glutReshapeFunc( Reshape );
//Rejestracja funkcji odpowiedzialnej za obsluge 
//klawiatury (tylko znaki ASCII)
glutKeyboardFunc( Key );
//Rejestracja funkcji odpowiedzialnej za obsluge 
//klawiatury (klawisze funkcyjne + strzalki + kl. specjalne)
glutSpecialFunc( SpecialKey );
//Rejestracja funkcji odpowiedzialnej za wyswietlanie
//osci okna
glutDisplayFunc( Display );
//Nasza wlasna inicjalizacja
Init();
//Wlaczenie przetwarzania zdarzen przez biblioteke GLUT
glutMainLoop();
return 0;
}

