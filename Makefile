CC=g++
ASMBIN=nasm

all : asm cc link
asm : 
	$(ASMBIN) -o tor.o -f elf64  tor.asm
cc :
	$(CC) -m64 -c -O0 -Wall -std=c++0x -g main.cc -o  main.o
link :
	$(CC) -m64 -o test -lstdc++ -L/usr/X11R6/lib -lglut -lGLU -lGL -lXext -lX11 -lm main.o tor.o
clean :
	rm *.o
	rm test
	rm errors.txt	
