; Program symulujący tor lotu szybowca
; Patryk Szypulski
;
;
;	int tor (char* tablica, parametry* params)
;
;struct parametry {
;	int zarezerowowane		RSI
;	int szer;				RSI+4
;	int wys;				RSI+8		// szerokość i wysokość bitmapy w pikselach
;	int czestotliwosc;		RSI+12
;	double czas_probk;		RSI+16		// czas próbkowania i czestotliwosc wyświetlania
;	double vx_pocz;			RSI+24
;	double vy_pocz;			RSI+32		// początkowe prędkości pionowa i pozioma w px/s [skala 1m/px]    
;	int x_pocz;				RSI+40
;	int y_pocz;				RSI+44		// początkowe wartości położenia względem dolnego lewego rogu w px
;	double stala_oporu;		RSI+48
;	double stala_nosna;		RSI+56		// stałe skalujące siły działające na szybowiec
;}
section .data

	licznik:	dd	900000			; licznik wyswietlen, zabezpieczenie dla idealnej petelki
	grawitacja: dq	0.0, -9.11		; wektor stałej grawitacji
	stala_pomocnicza dq 1.0, -1.0 	; przyda się przy budowaniu stałych
	R_kolor:	db	0x22			; składowa R koloru trasy
	G_kolor:	db	0x88			; składowa R koloru trasy
	B_kolor:	db	0x44			; składowa R koloru trasy
section .text
	global tor

tor:
_start:
	;zachowujemy base pointer i ustawiamy nasz
	push rbp
	mov rbp, rsp
	
	;zachowujemy rejestry
	push rbx
	push r12
	push r13
	push r14
	push r15
;====================================================================;
;tu sie wszystko dzieje

	
;przygotowanie wektorow i skalarów
;-- -- -- -- --	-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
;wektor czasu	[T, T] gdzie T to czas symulacji (delta T)
	movlpd xmm15, [rsi+16]	;wpisuje do dolnej części xmm15 czas próbkowania
	movhpd xmm15, [rsi+16]	;to samo do górnej części
	
;wektor położenia
	cvtpi2pd xmm14, [rsi+40]	;zamienia 64bity z pamięci na dwie liczby double w rejestrze xmm14

;stałe
 ;wspólne dla obu stałych
	movlpd xmm10, [rsi+48]				; wrzucamy stałą siły oporu do dolnej części xmm11
	movhpd xmm10, [rsi+48]				; i do górnej też
	mulsd xmm10, [rsi+56]				; xmm10 = [A, AC] <- AC to dolna część rejestru
	mulpd xmm10, xmm15					; xmm10 = [AT, ATC]
	movupd xmm1, [stala_pomocnicza]		; wrzucamy wektor [-1, 1]
 ;stała y:	
	mulpd xmm10, xmm1					; xmm10 = [-AT, ATC] <- stała y-owa	
 ;stała x:
	pshufd xmm11, xmm10, 01001110b		; kopiujemy zawartość xmm10 do xmm11 zamieniając liczby miejscami
	mulpd xmm11, xmm1					; domnażamy górną część przez -1, ostatecznie xmm11= [-ATC, -AT]
	
	
;wektor prędkości
	movupd xmm12, [rsi+24]				; wrzucamy wektor [vy, vx] do xmm12 <- vy w górnej cześci
			
;moduł wektora prędkości (w górnej i dolnej części wektora jest skalar - moduł wektora prędkości)
	movupd xmm13, xmm12				;kopiujemy zawartość xmm12 do xmm13
	mulpd xmm13, xmm12				; xmm13 = [vy^2, vx^2] <-vy^2 w górnej części
	pshufd xmm1, xmm13, 01001110b	;kopiujemy zawartość xmm13 do xmm0 zamieniając vx z vy miejscami
	addpd xmm13, xmm1				; xmm13 = [vx^2+vy^2, vx^2+vy^2]
	sqrtpd xmm13, xmm13				; wyciągamy pierwiastek
	
;wektor grawitacji
	movupd xmm9, [grawitacja]		; ładuje wektor g
	mulpd xmm9, xmm15				; xmm9 = [-gT, 0]
	
;skalar początkowego położenia
	cvttsd2si rbx, xmm14			; wrzucamy do rbx szerokość w pikselach (int)
	pshufd xmm1, xmm14, 01001110b	; zamieniamy miejscami gorną i dolną połowę xmm14 i wrzucamy to do xmm1
	cvttsd2si rdx, xmm1				; wrzucamy aktualną długość do rdx 

	
;skalar szerokosc tablicy
	mov r8d, DWORD[rsi+4]

;skalar licznika wyświetleń
	mov ecx, DWORD[licznik]
	
;częstotliwość wyświetlania
	mov r9d, DWORD[rsi+12]

;licznik do kolejnego wyświetlenia
	mov r13, r9
;kolor
	mov r10b, BYTE [R_kolor]
	mov r11b, BYTE [G_kolor]
	mov r12b, BYTE [B_kolor]
;-- -- -- -- --	-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
;koniec przygotowań, zaczyna się część gdzie coś się dzieje

petla:

;czy licznik wyswietleń zerowy? To znaczy, że wyświetlono już max razy i koniec
	cmp rcx, 0
	je koniec
	
;liczymy nowe wartości
  
  ;predkosci
	
	;predkosc x
	movupd xmm2, xmm12				; xmm2 = predkości
	mulpd xmm2, xmm11				; xmm2 = prędkości*stałaX
	pshufd xmm3, xmm2, 01001110b	; zamiana miejsc w xmm2 i wrzucamy do xmm3
	addpd xmm2, xmm3				; xmm2 = [-AT( Cvy + vx), -AT( Cvy + vx)]
	mulpd xmm2, xmm13				; wynikowa predkość X
	movsd xmm4, xmm2				; wrzucamy wynik na dół rejestru z prędkością nową
	
	;to samo dla predkosci y
	movapd xmm2, xmm12				; xmm2 = predkości
	mulpd xmm2, xmm10				; xmm2 = prędkości*stałaY
	pshufd xmm3, xmm2, 01001110b	; zamiana miejsc w xmm2 i wrzucamy do xmm3
	addpd xmm2, xmm3				; xmm2 = [AT( Cvx - vy), AT( Cvx - vy)]
	mulpd xmm2, xmm13				; wynikowa predkość Y
	shufpd xmm4, xmm2, 00b			; wrzucamy wynik na górę rejestru z prędkością nową
	
	;dodajemy grawitację
	addpd xmm12, xmm4				; dodajemy nową prędkość do starej
	addpd xmm12, xmm9				; xmm12 = nowa prędkość (po dodaniu grawitacji)
	

  ;moduł
	movapd xmm13, xmm12				; kopiujemy zawartość xmm12 do xmm13
	mulpd xmm13, xmm12				; xmm13 = [vy^2, vx^2] <-vy^2 w górnej części
	pshufd xmm1, xmm13, 01001110b	; kopiujemy zawartość xmm13 do xmm1 zamieniając vx z vy miejscami
	addpd xmm13, xmm1				; xmm13 = [vx^2+vy^2, vx^2+vy^2]
	sqrtpd xmm13, xmm13				; wyciągamy pierwiastek
	
  ;położenia
	movapd xmm1, xmm12				; xmm1 = prędkość
	mulpd xmm1, xmm15				; xmm1 = predkość*czas
	addpd xmm14, xmm1				; xmm14 = nowe położenia

  ;obcięte do intów nowe położenia
	cvttsd2si rbx, xmm14			; wrzucamy do rbx szerokość w pikselach (int)
	pshufd xmm1, xmm14, 01001110b	; zamieniamy miejscami gorną i dolną połowę xmm14 i wrzucamy to do xmm1
	cvttsd2si rdx, xmm1				; wrzucamy aktualną długość do rdx 

;czy poza obrazkiem? Jak tak to koniec
	
	cmp rbx, r8					;porównujemy rbx do szerokości tablicy
	jge koniec						;jak  0 < rbx < X 
									;to idź dalej, jak nie to koniec	
	cmp rbx, 0
	jl koniec
	
	cmp edx, DWORD[rsi+8]			;porównujemy rdx do wysokości tablicy
	jge koniec						;jak  0 < rdx < Y 
									;to idź dalej, jak nie to koniec	
	cmp rdx, 0
	jl koniec	
			
;czy trzeba wyświetlić? jak tak to to robimy i zerujemy licznik do nast. wyświetlenia

	cmp r13, 0 
	je wyswietl
	
	;jak mniejsze to nie wyswietlaj
	dec r13				;obniżamy licznik do wyświetlenia
	
	jmp petla
	
wyswietl:
	dec rcx							;dekrementuj licznik wyswietlen
	mov r13, r9
	
	mov r14, r8						; r14 = szerokość_tablicy
	imul r14, rdx					; r14 = szerokość*obecna_wysokość
	add r14, rbx					; r14 = szerokość*obecna_wys+szerokosc
	mov r15, r14
	shl r14, 1						; r14 = położenie w tablicy gdzie zaczyna się piksel
	add r14, r15
wstaw:
	mov BYTE [rdi+r14], r10b			; wpisz ustalony kolor pod wskazanym adresem
	mov BYTE [rdi+r14+1], r11b
	mov BYTE [rdi+r14+2], r12b
	
	jmp petla
	
koniec:
;====================================================================;
;tu przywracamy stan sprzed wywolania
	
	;przywracamy rejestry
	pop r15
	pop r14
	pop r13
	pop r12
	pop rbx
	
	;przywracamy stary base pointer
	pop rbp
	
	ret


